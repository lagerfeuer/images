#!/bin/bash

if [[ -z $1 ]]; then
  echo "Usage: $0 <dir>"
  exit 1
fi

DIR="$1"
if [[ ! -d "$DIR" ]]; then
  echo "$DIR is not a directory!"
  exit 1
fi

URL=registry.gitlab.com/lagerfeuer/images

docker build -t ${URL}/${DIR} ${DIR}/ \
  && docker push ${URL}/${DIR}

