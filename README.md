# Images

## clang-gcc
Installed: clang[++]-7 (aliased to clang[++]), gcc (g++), make, cmake, git

## clang-cross
Installed: clang[++]-7 with clang cross compiler for windows, [`wclang`](https://github.com/tpoechtrager/wclang).

